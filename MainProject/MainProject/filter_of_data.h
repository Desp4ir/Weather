#ifndef FILTER_OF_DATA_H
#define FILTER_OF_DATA_H

#include <iostream>
#include "weather.h"

using namespace std;

double month(DATA* array[], int size);
bool truth(DATA* item, int k);
int choice(void);
void output(DATA* array);
void del (DATA* array[], int size);
#endif

