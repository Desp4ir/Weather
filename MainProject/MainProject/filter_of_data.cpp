#include "filter_of_data.h"
#include "weather.h"
#include <cstring>
#include <iostream>
#include <string>

using namespace std;


double month(DATA* array[], int size)
{
	int k;
	double z = 0;
	cout << "month:" << endl;
	cin >> k;
	if (k > 0 && k <= 12) {
		for (int i = 0; i < size; i++) {
			if (truth(array[i], k)) {
				z += array[i]->info.DATA;
			}
		}
		if (z == 0) {
			cout << "     ";
			return 0;
		}
		return z;
	}
	else {
		cout << " " << endl;
		month(array, size);
	}
}

bool truth(DATA* item, int k) {
	if (item->time.month == k) {
		return true;
	}
	return false;
}

int choice() {
	cout << "  :  (1),  1.5(2)    (3)" << endl;
	int k;
	cin >> k;
	if (k == 1 || k == 2 || k == 3) {
		return k;
	}
	cout << " ." << endl;
	return choice();
}

void output(DATA* array) {


	cout << array->time.day << ".";
	cout << array->time.month << " ";
	cout << array->info.DATA << " ";
	cout << array->info.type << " ";

	cout << '\n';

}

void del(DATA* array[], int size) {
	int k;
	k = choice();

	if (k == 1) {
		string str;
		cin >> str;
		for (int j = 0; j < size; j++) {
			string s = array[j]->info.type;
			if (str == s) {
				output(array[j]);
			}
		}

	}
	else if (k == 2) {
		for (int j = 0; j < size; j++) {
			if (array[j]->info.DATA <= 1.5) {
				output(array[j]);
			}
		}
	}
	else if (k == 3) {
		cout << month(array, size);
	}
	else {
		cout << "no criterion";
	}
}
