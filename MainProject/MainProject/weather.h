#ifndef WEATHER_H
#define WEATHER_H

#include "constants.h"

using namespace std;

struct date {
    int day;
    int month;
};
struct rain {
    double DATA;
    char type[MAX_STRING_SIZE];
};

struct DATA {
    date time;
    rain info;
};

#endif

