#ifndef FILE_READER_H
#define FILE_READER_H

#include "weather.h"

void read(const char* file_name, DATA* array[], int& size);

#endif

