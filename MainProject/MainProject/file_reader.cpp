#include "file_reader.h"
#include "constants.h"
#include <fstream>
#include <cstring>
#include <string>
#include <iostream>


using namespace std;

date convert(char* str)
{
    date result;
    char* context = NULL;
    char* str_number = strtok_s(str, ".", &context);
    result.day = atoi(str_number);
    str_number = strtok_s(NULL, ".", &context);
    result.month = atoi(str_number);
    return result;
}


void read(const char* file_name, DATA* array[], int& size) {

    std::ifstream file(file_name);
    if (file.is_open())
    {
        int i = 0;
        char tmp_buffer[MAX_STRING_SIZE];


        while (!file.eof()) {

            DATA* item = new DATA;
            char k;

            file >> tmp_buffer;
            item->time = convert(tmp_buffer);
            file >> item->info.DATA;
            file.getline(item->info.type, MAX_STRING_SIZE);

            array[i] = item;
            i++;
        }
        size = i;
        file.close();
    }
    else
    {
        throw "  ";
    }
}
